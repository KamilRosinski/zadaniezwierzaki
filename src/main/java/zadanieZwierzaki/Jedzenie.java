package zadanieZwierzaki;

public class Jedzenie {
private int iloscJedzenia;
private RodzajJedzenia rodzajJedzenia;

    public Jedzenie(int iloscJedzenia, RodzajJedzenia rodzajJedzenia) {
        this.iloscJedzenia = iloscJedzenia;
        this.rodzajJedzenia = rodzajJedzenia;
    }

    public int getIloscJedzenia() {
        return iloscJedzenia;
    }

    public RodzajJedzenia getRodzajJedzenia() {
        return rodzajJedzenia;
    }

    public void setIloscJedzenia(int iloscJedzenia) {
        this.iloscJedzenia = iloscJedzenia;
    }

    public void setRodzajJedzenia(RodzajJedzenia rodzajJedzenia) {
        this.rodzajJedzenia = rodzajJedzenia;
    }

    public void zwiekszIliosc(int iloscDoDodania) {
        iloscJedzenia = iloscJedzenia + iloscDoDodania;
    }
}
