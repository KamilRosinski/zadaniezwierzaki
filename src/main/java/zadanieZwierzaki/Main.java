package zadanieZwierzaki;

       /* Napisz aplikację która ma za zadanie stworzyć osobę która będzie mogła posiadać zwierzaki różnego rodzaju.
        Każdy zwierzak ma swoją listę ulubionych potraw. Osoba ma listę dostępnych potraw. Nakarm wszystkie zwierzaki.*/


import zadanieZwierzaki.mojeZwierzaki.Kot;
import zadanieZwierzaki.mojeZwierzaki.Zwierzak;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Zwierzak zwierzak = new Kot("Mruczek");
        zwierzak.dodajPosilek(new Jedzenie(2,RodzajJedzenia.Mleko));
        zwierzak.dodajPosilek(new Jedzenie(3,RodzajJedzenia.Ryba));


        System.out.println(zwierzak.dajGlos());
        System.out.println(zwierzak.przedstawSie());


    }
}
