package zadanieZwierzaki.mojeZwierzaki;

import zadanieZwierzaki.Jedzenie;

import java.util.ArrayList;
import java.util.List;

public class Kot extends Zwierzak {

    public Kot(String imie) {
        super(imie);
    }

    @Override
    public String dajGlos() {
        return "miaaaał miaaaał";
    }

    @Override
    protected String kimJestes() {
        return "kotem";
    }
}
