package zadanieZwierzaki.mojeZwierzaki;

import zadanieZwierzaki.Jedzenie;

import java.util.List;

public class Pies extends Zwierzak {
    public Pies(String imie) {
        super(imie);
    }

    @Override
    public String dajGlos() {
        return "hau hau hau";
    }

    @Override
    protected String kimJestes() {
        return "psem";
    }
}
