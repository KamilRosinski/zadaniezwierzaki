package zadanieZwierzaki.mojeZwierzaki;

import zadanieZwierzaki.Jedzenie;

import java.util.List;

public class Ptak extends Zwierzak{
    public Ptak(String imie) {
        super(imie);
    }

    @Override
    public String dajGlos() {
        return "cwir cwir";
    }

    @Override
    protected String kimJestes() {
        return "ptakiem";
    }
}
