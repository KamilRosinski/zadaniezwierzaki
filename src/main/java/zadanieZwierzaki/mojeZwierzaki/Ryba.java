package zadanieZwierzaki.mojeZwierzaki;

import zadanieZwierzaki.Jedzenie;

import java.util.List;

public class Ryba extends Zwierzak {
    public Ryba(String imie) {
        super(imie);
    }

    @Override
    public String dajGlos() {
        return "bul bul";
    }

    @Override
    protected String kimJestes() {
        return "rybą";
    }
}
