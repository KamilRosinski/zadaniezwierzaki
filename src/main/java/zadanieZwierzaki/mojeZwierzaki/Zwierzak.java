package zadanieZwierzaki.mojeZwierzaki;

import zadanieZwierzaki.Jedzenie;

import java.util.ArrayList;
import java.util.List;

public abstract class Zwierzak {
    String imie;
    List<Jedzenie> listaPosilkow;

    public Zwierzak(String imie) {
        this.imie = imie;
        listaPosilkow =new ArrayList<>();
    }

    public abstract String dajGlos();

    public String przedstawSie(){
        StringBuilder strBild =new StringBuilder();

        strBild.append("Nazywam się ").append(imie).append(" i jestem: ").append(kimJestes());

        return strBild.toString();
    }

    protected abstract String kimJestes();

    public void dodajPosilek(Jedzenie posilek){
        int indexPosilku = listaPosilkow.indexOf(posilek);

        if (indexPosilku!=-1){
            Jedzenie tenZwieksz = listaPosilkow.get(indexPosilku);
//brzydka metoda // tenZwieksz.setIloscJedzenia(tenZwieksz.getIloscJedzenia()+posilek.getIloscJedzenia());

            tenZwieksz.zwiekszIliosc(posilek.getIloscJedzenia());
        }else
        listaPosilkow.add(posilek);
    }

}
